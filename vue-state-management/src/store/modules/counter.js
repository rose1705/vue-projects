import * as types from '../types';

const state = {
    counter: 0,
}

const getters = {
     //state is always passed as prop
     [types.DOUBLE_COUNTER]: ({ counter }) => {
        return counter * 2;
    },
    [types.CLICK_COUNTER]: state => {
        return state.counter + ' Clicks';
    },
}

const mutations = {
    increment: (state, payload) => {
        state.counter += payload;
    },
    decrement: (state, payload) => {
        state.counter -= payload;
    },
}

const actions = {
    increment: (context, payload) => {
        context.commit('increment', payload);
    },
    //use destructuring to take commit function out of context
    decrement: ({ commit }, payload) => {
        commit('decrement', payload)
    },
    asyncIncrement: ({commit}, payload) => {
        setTimeout( () => {
            commit('increment', payload.by);
        }, payload.duration);
    },
    asyncDecrement: ({commit}, payload) => {
        setTimeout(() => {
            commit('decrement', payload);
        }, 1000);
    },
}

export default {
    state, getters, mutations, actions
}