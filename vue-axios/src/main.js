import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import store from './store'

axios.defaults.baseURL = 'https://vuejs-axios-3d60b.firebaseio.com';
// axios.defaults.headers.common['Accepts'] = 'application/json';


// const requestInterceptor = axios.interceptors.request.use(config => {
//   console.log('Request interceptor', config);
//   return config;
// })

// axios.interceptors.response.use(res => {
//   console.log('response interceptor', res);

//   return res;
// })


// axios.interceptors.request.eject(requestInterceptor);

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
